from django.contrib import admin
from .models import WaterHole,ZoneModel,Comitte
# Register your models here.
admin.site.register(WaterHole)
admin.site.register(ZoneModel)
admin.site.register(Comitte)